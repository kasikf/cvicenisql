<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seznam krajů</title>
</head>
<body>
    <form action="" method="get">
        <div>
            <label for="kraj">Vyberte kraj:</label>
            <select name="kraj" id="kraj">
                <option value="">Vyberte kraj</option>
                <?php
                $sql = "SELECT * FROM kraj";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                  while ($row = $result->fetch_assoc()) {
                    echo "<option value='$row[id]'>$row[nazev]</option>";
                  }
                }
              ?>
            </select>
        </div>

        <div>
            <label for="kraj">Vyberte okres:</label>
            <select name="okres" id="okres">
                <option value="">Vyberte okres</option>
                <?php
                $kraj = $_GET['kraj'];
                $sql = "SELECT * FROM okres WHERE kraj_id = $kraj";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                    echo "<option value='$row[id]'>$row[nazev]</option>";
                    }
                }
                ?>
            </select>
        </div>
            
    </form>

    <?php
    $okres = $_GET['okres'];
    $sql = "SELECT * FROM okres WHERE kraj_id = $kraj AND id = $okres";

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
        echo "$row";
        }
    }
    ?>
</body>
</html>