<?php
/* 
  Doplňte kód tak, aby zobrazil z DB seznam okresů dle id kraje. Id kraje doplňte do SQL na pevno, 
  není potřeba zadávat pomocí formuláře
*/

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";
$krajId = 1;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql="SET CHARACTER SET UTF8";
$conn->query($sql);

$sql = "SELECT * FROM okres WHERE kraj_id = $krajId";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    echo "<div>$row[nazev]</div>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>