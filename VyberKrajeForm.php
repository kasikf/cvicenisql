<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Seznam krajů</title>
</head>
<body>

    <!-- Vytvořte formulář, kde uživatel vybere kraj a id kraje se odešle ke zpracovnání 
        do vypisSeznamOkresuForm.php -->

    <form action="vypisSeznamOkresuForm.php" method="get">
        <div>
            <label for="kraj">Vyberte kraj:</label>
            <select name="kraj" id="kraj">
              <option value="">Vyberte kraj</option>
              <?php
              $sql = "SELECT * FROM kraj";
              $result = $conn->query($sql);

              if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                  echo "<option value='$row[id]'>$row[nazev]</option>";
                }
              }
              ?>
            </select>
            <input name="krajForm" type="submit" value="Odeslat">
        </div>
    </form>
</body>
</html>